# Moore Neighbourhoods
A basic JavaScript implementation of the Moore neighbourhood tracing algorithm.

You can click on an image and the area you clicked on is highlighted with a red outline. It is a way of doing edge detection essentially. Something like this could be used for the classic Paint 'fill' tool, or the Photoshop magic wand maybe.

This is just a single HTML page and a sample image, so it should work for anyone with a browser.